import os, random
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from manage import Entries, Stem, ZoneInfo, Weather

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
db = SQLAlchemy(app)

app.config.update(dict(
    DEBUG=True,
	SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
	
for zoneNum in range(0,8):
	# popular database with 8 zones and varying conditions
	zoneConnected = True
	stemPresent = True
	waterAmount = 1.5
	waterInterval = 3
	waterIndex = 1.5
	sprinkler_output = 1.5
	# need to calculate threshold, duration, decay
	zone_data = ZoneInfo(zone=zoneNum, zone_connected=zoneConnected, sprinkler_output=sprinkler_output,
		stem_present=stemPresent, water_amount=waterAmount, water_interval=waterInterval, water_index=waterAmount)
	db.session.add(zone_data)
		
db.session.commit()

print "got here"
