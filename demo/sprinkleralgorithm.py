import sys
sys.path.insert(0, '/home/pi/bloom/main_controller/source/web')
from sqlite3 import dbapi2 as lite
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from manage import ZoneInfo, SysSettings
from bloom_i2c import PCA9555
import time

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////var/www/app.db'
db = SQLAlchemy(app)
ioExpander = PCA9555(0x20)
ioExpander.setInputDirection(0x0000)

def executeZone(port, zoneNum, duration):
	print "port:", port, "Starting zone", zoneNum+1, "for", duration, "seconds"
	ioExpander.turnOn(port, zoneNum)
	time.sleep(duration)
	ioExpander.turnOff(port, zoneNum)

def main():
	print "\n----Starting demo sprinkler timing algorithm----"
	port = '1'
	zoneNum = 4
	progDelay = 0
	query = db.session.query(SysSettings)
	result = query.first()
	progDelay = result.delay * 60
	print "Delay time:", progDelay
	if(progDelay != 0):
		time.sleep(progDelay)
		query.update({"delay":0})
		db.session.commit()
	query = db.session.query(ZoneInfo)
	result = query.all()
	for instance in result:
		executeZone(port, instance.zone, instance.water_duration)
	return
	
main()

print "------------------Program End-------------------\n"
