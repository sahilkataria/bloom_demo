import smbus
import time

class PCA9555():
	#open Linux device /dev/i2c-1
	i2c = smbus.SMBus(1)

	def __init__(self, addr):
		self.addr = addr

	def writeRegisterPair(self, reg, value):
		low = value & 0xff
		high = (value >> 8) & 0xff 
		self.i2c.write_byte_data(self.addr, reg, low)
		self.i2c.write_byte_data(self.addr, reg + 1, high)

	def setInputDirection(self, value):
		self.writeRegisterPair(6, value)

	def setOutput(self, value):
		self.writeRegisterPair(2, value)

	def readByte(self, port, pin):
		val = -1
		if port == '0':
			val = self.i2c.read_byte_data(self.addr, 2)
		if port == '1':
			val = self.i2c.read_byte_data(self.addr, 3)

		return val

	def turnOff(self, port, pin):
		val = self.readByte(port, pin)
		if port == '0':
			val |= (1 << pin)
			self.i2c.write_byte_data(self.addr, 2, val)
		if port == '1': 
			val |= (1 << pin)
			self.i2c.write_byte_data(self.addr, 3, val)

	def turnOn(self, port, pin):
		val = self.readByte(port, pin)
		if port == '0':
			val = self.i2c.read_byte_data(self.addr, 2)
			val &= ~(1 << pin)
			self.i2c.write_byte_data(self.addr, 2, val)
		if port == '1':
			val = self.i2c.read_byte_data(self.addr, 3)
			val &= ~(1 << pin)
			self.i2c.write_byte_data(self.addr, 3, val)

def main():
 	ioExpander=PCA9555(0x20)   
	ioExpander.setInputDirection(0x0000)
	ioExpander.setOutput(0x0000)

	port=raw_input("enter port number:") 
	pin=int(raw_input("enter pin number:"))
	print pin, port
	ioExpander.turnOn(port,pin)
	time.sleep(3)
	ioExpander.turnOff(port,pin)

if __name__ == "__main__":
	main()
