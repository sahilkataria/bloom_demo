import os, pyowm, random
from sqlite3 import dbapi2 as lite
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from manage import Entries, Stem, ZoneInfo, Weather

class updateZone():
	
	def update_zones(self):
		app = Flask(__name__)
		app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
		db = SQLAlchemy(app)
		conn = lite.connect('app.db')
		conn.row_factory = lite.Row
		cur = conn.cursor()
		cur2 = conn.cursor()
	
		app.config.update(dict(
		    DEBUG=True,
		    SECRET_KEY='development key',
		    USERNAME='admin',
		    PASSWORD='default'
		))
		
#		cur2.execute("SELECT precip FROM weather WHERE ID = (SELECT MAX(ID) FROM weather")
		cur2.execute("SELECT * FROM weather ORDER BY id DESC LIMIT 1")
		rowWeather = cur2.fetchone()
		
		for zoneNum in range(0,8):
			result = ZoneInfo.query.filter_by(zone=zoneNum).first()
			cur.execute("SELECT * FROM zone_info WHERE zone=?", (zoneNum,))
			row = cur.fetchone()
			
			# check if zone is connected
			if(result.zone_connected == True):
				# update decay value
				waterDecay = row["water_amount"] / 7
				conn.execute("UPDATE zone_info set water_decay=? where zone=?", (waterDecay, zoneNum))
				# decay the index prior to sprinkler program execution
				# (0.001 is there to round below threshold)
				waterIndex = row["water_index"] - waterDecay - 0.001 + rowWeather["precip"]
				conn.execute("UPDATE zone_info set water_index=? where zone=?", (waterIndex, zoneNum))
				# update the index threshold
				waterThreshold = row["water_amount"] - row["water_amount"] * row["water_interval"] / 7
				conn.execute("UPDATE zone_info set water_threshold=? where zone=?", (waterThreshold, zoneNum))
				# update program zone sprinkler run time (change '3' to '60' to convert duration to minutes) 
				waterDuration = (row["water_amount"] - waterIndex) / row["sprinkler_output"] * 3
				conn.execute("UPDATE zone_info set water_duration=? where zone=?", (waterDuration, zoneNum))

		conn.commit()
		conn.close()

# update zones when this file is executed
updateZone = updateZone()
updateZone.update_zones()
print "got here"
