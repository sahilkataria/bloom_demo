import os, pyowm, random
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from manage import Entries, Stem, ZoneInfo, SysSettings
from bloom_i2c import PCA9555
from bloom_scheduler import BloomScheduler
import json, time

owm = pyowm.OWM('d6435fb0468f5146a3bd962d141bc643')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
db = SQLAlchemy(app)
ioExpander = PCA9555(0x20)   
ioExpander.setInputDirection(0x0000)
#ioExpander.setOutput(0x1111)

# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

@app.route('/')
def show_entries():
    result = Stem.query.all()
    return render_template('show_entries.html', entries=result)

@app.route('/add', methods=['GET'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)

    moist = random.randrange(1, 100, 1)
    observation = owm.weather_at_place('San Francisco,US')
    w = observation.get_weather()
    temp_obj = w.get_temperature('fahrenheit')
    temp = temp_obj['temp_max']
    stem_data = Stem(moisture=moist, temperature=temp)
    db.session.add(stem_data)
    db.session.commit()

    post_title = request.form['title']
    post_text = request.form['text']
    entry = Entries(title=post_title, text=post_text)
    db.session.add(entry)
    db.session.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))

@app.route('/app/home')
def app_home():
    system = db.session.query(SysSettings).first()
    observation = owm.weather_at_place(system.location)
    w = observation.get_weather()
    temp_obj = w.get_temperature('fahrenheit')
    temp = temp_obj['temp_max']
    icon_name = w.get_weather_icon_name()
    icon_url = "http://openweathermap.org/img/w/" + icon_name + ".png"
    status = w.get_detailed_status()
    humidity = w.get_humidity()
    rain = w.get_rain()
    if '3h' in rain:
        rain_vol = rain['3h']
    else:
        rain_vol = 0

    # Variables for homepage - Next cycle due in 2 days on May 2, 2016 at 3AM
    hours, minutes = system.start_time.split(":")
    if int(hours) == 0:
        days_left = 0
        date = time.strftime("%b %d, %Y")
        hh = int(time.strftime("%I"))
        mm = (int(time.strftime("%M")) + system.delay + int(minutes))
        if mm >= 60:
            mm = mm - 60
            hh = hh + 1
        if mm < 10:
            mm = "0" + str(mm)
        ap = time.strftime("%p")
        set_time = str(hh) + ":" + mm + " " + ap
    else:
        date = "May 17, 2016"
        set_time = system.start_time
        days_left = str(system.interval)

    delay = system.delay if system.delay > 0 else ''

    return render_template('index.html', weather_icon=icon_url, temperature=temp, weather_status=status, \
        humidity=humidity, rain_volume=rain_vol, interval=days_left, next_date=date, start_time=set_time, city=system.location, delay=delay)

@app.route('/app/settings', methods = ['GET', 'POST'])
def app_settings():
    if request.method == 'POST':
        delay_time = request.form.get('delay', None)
        settings = db.session.query(SysSettings).filter_by()

        if delay_time:
            delay_time = int(delay_time)
            settings.update({"delay": delay_time}) 
        else:
            start_time = request.form['start_time'] 
            interval = filter(lambda x: x.isdigit(),request.form['interval'])
            #delay = filter(lambda x: x.isdigit(),request.form['delay'])
            location = request.form['location']
            settings.update({"start_time":start_time, "interval": interval, "location": location})

            # UPDATE CRON HERE
            hour, minute = (int(x) for x in start_time.split(":"))
            schedule = BloomScheduler()
            schedule.delete()

            # IF CONDITION IS SJSU EXPO HACK!!!
            if hour == 0 and int(interval) == 1:
                schedule.add(hour = '*', minute = '*/' + str(minute), interval = '*')
            else:
                schedule.add(hour = hour, minute = minute, interval = interval)
        
        db.session.commit()
        return redirect(url_for('app_home'))

    return render_template('system-settings.html')

json_str = '''[{"title": "John", "time": "Smith"}, {"title": "Jane", "time": "Heart"}, {"title": "Dome",
    "time": "Robinsons"}, {"title": "Pete", "time": "Hand"},{"title": "Pete", "time": "Hand"}]'''
@app.route('/app/zones')
def app_zones():
    result = ZoneInfo.query.all()
    zones = {}
    for row in result:
        zones[row.zone] = row.zone_name

    return render_template('zones.html', zones = zones)

@app.route('/app/zones/settings')
def app_zones_settings():
    return render_template('zones_settings.html')

@app.route('/app/zones/delete', methods = ['GET', 'POST'])
def app_zones_del():
    if request.method == "POST":
        zone_number = request.form['zone']
        db.session.query(ZoneInfo).filter_by(zone = zone_number).delete()
        db.session.commit()
        return redirect(url_for('app_zones'))

    result = ZoneInfo.query.all()
    zones = {}
    for row in result:
        zones[row.zone] = row.zone_name

    return render_template('delete.html', zones = zones)

@app.route('/app/zones/add', methods = ['GET', 'POST'])
def app_zones_add(): 
    if request.method == 'GET':
        channel = request.args.get('channel')
        query = db.session.query(ZoneInfo).filter_by(zone = channel)
        result = query.first()
        if result:
            data = {}
            data['name'] = result.zone_name
            data['sprinkler_head'] = result.sprinkler_head
            return jsonify(data)
        else:
            return render_template('add.html')
    else:
        channel = request.form['channel']
        zone_name = request.form['zname']
        sprinkler_head = request.form['spr_head']
        if sprinkler_head == 'bubbler':
            sprinkler_output = 2.5
        elif sprinkler_head == 'drip':
            sprinkler_output = 0.5
        elif sprinkler_head == 'rotary':
            sprinkler_output = 0.5
        elif sprinkler_head == 'spray':
            sprinkler_output = 1.5
        else:
            sprinkler_output = 1.5

        water_amount = 1.5
        water_interval = 3
        water_index = 1.5

        query = db.session.query(ZoneInfo).filter_by(zone = channel)
        result = query.first()

        if result:
            query.update({"zone_name":zone_name, "sprinkler_head":sprinkler_head, "sprinkler_output":sprinkler_output, \
                "water_duration":(sprinkler_output * 2)})
        else:
            new_zone = ZoneInfo(zone = channel, zone_name = zone_name, zone_connected=True, sprinkler_output = sprinkler_output, stem_present = False, \
                water_amount = water_amount, water_interval = water_interval, water_index = water_index, water_duration = (sprinkler_output * 2), \
                sprinkler_head = sprinkler_head)
            db.session.add(new_zone)

        db.session.commit()

        return redirect(url_for('app_zones'))

    return render_template('add.html')

@app.route('/app/zones/settings/configure')
def app_zones_settings_configure():
    return render_template('configure.html')

@app.route('/app/zones/settings/setup')
def app_zones_settings_setup():
    return render_template('setup.html')

@app.route('/app/zones/settings/notification')
def app_zones_settings_notification():
    return render_template('notification.html')

@app.route('/app/zones/settings/controller')
def app_zones_settings_controller():
    return render_template('controller.html')

@app.route('/app/stats')
def app_stats():
    return render_template('stats.html')

@app.route('/app/stats-weekly')
def app_stats_weekly():
    return render_template('stats-weekly.html')

@app.route('/app/stats-monthly')
def app_stats_monthly():
    return render_template('stats-monthly.html')

@app.route('/app/stats-yearly')
def app_stats_yearly():
    return render_template('stats-yearly.html')


@app.route('/ioexp/', methods=['GET', 'POST'])
def ioexp():
    if request.method == 'POST':
        if not request.json or not 'port' in request.json or not 'pin' in request.json or not 'state' in request.json:
            return jsonify({'error': 'Please provide all values: pin, port, state!'}), 400

        port = request.json['port']
        pin = int(request.json['pin'])

        if port not in ['0', '1'] or pin not in [0, 1, 2, 3, 4, 5, 6, 7]:
            return jsonify({'error': 'Value of port/pin undefined! Use 0 - 1 for port, 0 - 7 for pin.'}), 400

        state = int(request.json['state'])
        if state == 1:
            ioExpander.turnOn(port, pin)
            return jsonify({'info': 'IO port turned on!'})
        elif state == 0:
            ioExpander.turnOff(port, pin)
            return jsonify({'info': 'IO port turned off!'})
        else:
            return jsonify({'error': 'Value of state undefined! Use 0 or 1.'}), 400

    else:
        return jsonify({'info': 'GET request!'})

if __name__ == '__main__':
    app.run(host='0.0.0.0')