import smbus
import time
from bloom_i2c import PCA9555

def main():
 	ioExpander=PCA9555(0x20)   
	ioExpander.setInputDirection(0x0000)
	ioExpander.setOutput(0x0000)

	for port in '01':
		for pin in range(0,8):
			ioExpander.turnOff(port,pin)

if __name__ == "__main__":
	main()
