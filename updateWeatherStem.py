import os, pyowm, random
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from manage import Entries, Stem, ZoneInfo, Weather

class outsideData():
	
	def update_data(self):
		# Load default config and override config from an environment variable
		owm = pyowm.OWM('d6435fb0468f5146a3bd962d141bc643')
	
		app = Flask(__name__)
		app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
		db = SQLAlchemy(app)
	
		app.config.update(dict(
		    DEBUG=True,
		    SECRET_KEY='development key',
		    USERNAME='admin',
		    PASSWORD='default'
		))
		
		for zoneNum in range(1,9):
			result = ZoneInfo.query.get(zoneNum)
			# check if zone and stem connected
			if(result.zone_connected == True and result.stem_present == True):
				# update stem info for each zone (temp taken from owm for now)
				moist = random.randrange(1, 100, 1)
				observation = owm.weather_at_place('san francisco, us')
				w = observation.get_weather()
				temp_obj = w.get_temperature('fahrenheit')
				temp = temp_obj['temp_max']
#				print zoneNum, "Temp:", temp
#				print "Moisture:", moist
#				print " "
				stem_data = Stem(zone=(zoneNum-1), moisture=moist, temperature=temp)
				db.session.add(stem_data)
		
		# update weather info
		w = observation.get_weather()
		temp_obj = w.get_temperature('fahrenheit')
		temp = temp_obj['temp_max']
		status = w.get_detailed_status()
		humidity = w.get_humidity()
#		print "Humidity:", humidity
		rain = w.get_rain()
		if '3h' in rain:
			rain_vol = rain['3h']
		else:
			rain_vol = 0
#		print "Rain 3h:", rain_vol
		weather_data = Weather(temperature=temp, precip=rain_vol, humidity=humidity)
		db.session.add(weather_data)
		db.session.commit()

outsideData = outsideData()
outsideData.update_data()
print "Program end"
