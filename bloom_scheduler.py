from crontab import CronTab
import sys

class BloomScheduler:
	def __init__(self, userN=True):
		self.tab = CronTab(user=userN)

	def	add(self, cmd = 'python /var/www/demo/sprinkleralgorithm.py', comment='Bloom command', hour='*', minute='*', interval=1):
		cron_job = self.tab.new(cmd, comment)
		time_string = "*/" + str(interval)
		cron_job.setall(minute, hour, time_string, None, None)
		self.tab.write()
		print self.tab.render()

	def delete(self, cmd = 'python /var/www/demo/sprinkleralgorithm.py', comment='Bloom command'):
		cron_job = self.tab.find_command(cmd)
		if all(False for _ in cron_job):
			print "No command found!"
		else:
			self.tab.remove_all(command = cmd)
			self.tab.write()
			print "Command found and deleted!"

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print "Invalid number of arguments! Say add or delete"
		sys.exit()

	command = 'echo "hello" > /dev/ttys003'
	cronjob = BloomScheduler()

	if sys.argv[1] == 'add':
		# Add new command
		cronjob.add(cmd = command)
		sys.exit()

	if sys.argv[1] == 'delete':
		# Delete old command
		cronjob.delete(cmd = command)