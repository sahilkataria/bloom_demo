import os, pyowm, random
import sqlite3 as db

from flask import Flask
from sqlalchemy import DateTime
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from sqlalchemy.sql import func

conn = db.connect('app.db')

print "Database opened successfully!"

cursor = conn.execute("SELECT id, time, moisture, temperature from Stem")
for row in cursor:
   print "ID = ", row[0]
   print "TIME = ", row[1]
   print "MOISTURE = ", row[2]
   print "TEMP = ", row[3], "\n"

print "Operation done successfully";

db.create_all()
db.session.commit()

#admin = User('admin', 'admin@example.com')
#guest = User('guest', 'guest@example.com')
#db.session.add(admin)
#db.session.add(guest)
#db.session.commit()
#users = User.query.all()
#print users

conn.close()




