import random
import time
import threading

# Checks if zone is connected
def isZoneConn():
    randVal = random.choice([True, False])
    return randVal

# Checks database for calculated zone watering duration
def retrieveDuration(zoneNum):
    print("\tRetrieving zone duration for zone " + str(zoneNum) + ".")
    return 15
    
# Takes zone number and watering duration and queues background processes
def executeZone(zoneNum, duration):
    lock.acquire()
    time.sleep(3)
    #turnOn(zoneNum, duration)
    print("\tZone " + str(zoneNum) + " will turn on for " + str(duration) + " minutes.")
    lock.release()
    return

# Main algorithm
def main():
    print("Starting algorithm\n")
    lock = threading.Lock()
    
    # Perform check to enter default of custom mode
    # Check for internet connectivity, failover
    
    print("Entering default mode\n")
    for zoneNum in range(1,9):
        print("Zone " + str(zoneNum) + ":")
        if isZoneConn():
            print("\tZone " + str(zoneNum) + " is connected")
            zoneDuration = retrieveDuration(zoneNum)
            thread = threading.Thread(target=executeZone, args=(zoneNum, zoneDuration))
            thread.start()
            print("\tSPUN OFF THREAD")
        else:
            print("\tZone" + str(zoneNum) + " is NOT connected")
    return

main()
print("\n------Main DONE------\n")

for i in range (1,4):
    time.sleep(1)
    print("Look at me")
    time.sleep(1)
    print("I'm doing stuff")
    time.sleep(1)
    print("I'm a nonblocking BOSS")
    time.sleep(1)
    print("Heck yeah\n")
