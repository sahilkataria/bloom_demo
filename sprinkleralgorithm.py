
from sqlite3 import dbapi2 as lite
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from manage import ZoneInfo
from bloom_i2c import PCA9555
import time
from multiprocessing import Process, Lock

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
db = SQLAlchemy(app)
ioExpander = PCA9555(0x20)
ioExpander.setInputDirection(0x0000)
conn = lite.connect('app.db')
conn.row_factory = lite.Row
cur = conn.cursor()

def executeZone(lock, zoneNum):
	time.sleep(1)
	port = '1'
	result = ZoneInfo.query.filter_by(zone=zoneNum).first()

	lock.acquire()
#	print "\nLock acquired"	
	print "Starting zone", result.zone+1, "for", result.water_duration, "seconds"
	ioExpander.turnOn(port, result.zone)
	time.sleep(result.water_duration)
	ioExpander.turnOff(port, result.zone)
#	print "Releasing lock"
	lock.release()

def main():
#	port = '1'
	lock = Lock()

	print "\n------Starting basic sprinkler timing algorithm------"
	for zoneNum in range (0,8):
#		result = ZoneInfo.query.get(zoneNum)
		result = ZoneInfo.query.filter_by(zone=zoneNum).first()
		cur.execute("SELECT * FROM zone_info WHERE zone=?", (zoneNum,))
		row = cur.fetchone()
		
		if(result.zone_connected == True):
			if(result.water_index <= result.water_threshold):
#				print "Starting process for zone", result.zone
				p = Process(target=executeZone, args=(lock,zoneNum))
				p.start()
				waterIndex = result.water_amount
				conn.execute("UPDATE zone_info SET water_index=? WHERE zone=?", (waterIndex, zoneNum))
				
			else:
				print "Zone", zoneNum, "not ready to be watered yet."
	conn.commit()
	conn.close()
	
	return
	
main()

print "------Program End------\n"
