#!/usr/bin/env python

from flask import Flask
from sqlalchemy import DateTime
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from sqlalchemy.sql import func

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

class Entries(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(64))
    text = db.Column(db.String(256))

class Stem(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    time = db.Column(DateTime(timezone=True), server_default=func.now())
    zone = db.Column(db.Integer)
    moisture = db.Column(db.String(8))
    temperature = db.Column(db.String(256))

class Init(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    wifi_config = db.Column(db.Boolean, default=False)
    wifi_ssid = db.Column(db.String(32), nullable=True)
    wifi_pwd = db.Column(db.String(16), nullable=True)

class ZoneInfo(db.Model):
	zone = db.Column(db.Integer, primary_key = True)
	zone_name = db.Column(db.String(256))
	zone_connected = db.Column(db.Boolean)
	stem_present = db.Column(db.Boolean)
	sprinkler_head = db.Column(db.String(32))
	sprinkler_output = db.Column(db.Float)
	water_amount = db.Column(db.Float)
	water_interval = db.Column(db.Integer)
	water_threshold = db.Column(db.Float)
	water_duration = db.Column(db.Integer)
	water_index = db.Column(db.Float)
	water_decay = db.Column(db.Float)

class Weather(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	temperature = db.Column(db.Float)
	precip = db.Column(db.Float)
	humidity = db.Column(db.Float)

class SysSettings(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	start_time = db.Column(db.String(32) , nullable=False)
	interval = db.Column(db.Integer, nullable=False) # in days
	delay = db.Column(db.Integer, nullable=False) # in minutes
	location = db.Column(db.String(64), nullable=False)

db.create_all()

if __name__ == '__main__':
    manager.run()

